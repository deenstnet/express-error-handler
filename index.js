const Boom = require('@hapi/boom');
const { isCelebrate } = require('celebrate');
const _ = require('lodash');

module.exports = [
  // Default route
  (req, res, next) => next(Boom.notFound()),

  // Standard error logging middleware, must take 4 arguments
  function(e, req, res, next) {
    console.error(e);
    return next(e);
  },

  // Joi error handling middleware, must take 4 arguments
  function(e, req, res, next) {
    if (!isCelebrate(e)) return next(e);

    const { joi, meta } = e;

    e = Boom.badData(joi.message, {
      source: meta.source,
      keys: joi.details.map(detail => {
        return _.escape(detail.path.join('.'));
      })
    });

    return next(e);
  },

  // Boom error handling middleware, must take 4 arguments
  function(e, req, res, next) {
    if (!Boom.isBoom(e)) {
      if (e.name == 'MongoError' && e.code == 11000) {
        e = Boom.conflict();
      } else if (e.name == 'CastError') {
        e = Boom.badData();
      } else if (e.name == 'UnauthorizedError') {
        e = Boom.unauthorized();
      } else if (typeof e != Error) {
        e = Boom.badImplementation();
      } else {
        Boom.boomify(e);
      }
    }

    return res.status(e.output.statusCode).send({
      ...e.output.payload,
      ...(_.isNil(e.data) ? {} : { data: e.data })
    });
  }
];